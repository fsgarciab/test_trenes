/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Tests;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import trainsrouteinfo.exceptions.NotAllowedWeightException;
import trainsrouteinfo.graphs.directed.DirectedEdge;
import trainsrouteinfo.graphs.directed.DirectedGraph;
import trainsrouteinfo.graphs.directed.DirectedNode;

/**
 *
 * @author Excalibur
 */
public class TestDirectedGraph {
    
    public TestDirectedGraph() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

     @Test
     public void testDirectedNode() {
         DirectedNode directedNode_a = new DirectedNode("A");
         assertEquals(directedNode_a.getName(), "A");  
           
     }
     @Test
     public void testDirectedNodeConnections() {
        DirectedNode directedNode_a = new DirectedNode("A");
        DirectedNode directedNode_b = new DirectedNode("B");
        DirectedEdge directedEdge = new DirectedEdge(5,directedNode_b);
        directedNode_a.addEdge(directedEdge);
        assertEquals(directedNode_a.getEdges().get("B"),directedEdge ); 

     }
     
    @Test(expected = NotAllowedWeightException.class)
    public void testDirectedEdges() throws Exception {
        DirectedNode directedNode_b = new DirectedNode("B");
        new DirectedEdge(-5,directedNode_b);
    }
    
    
     @Test
     public void testDirectedGraph() {
       DirectedGraph directedGraph = new DirectedGraph();
       DirectedNode directedNode_a =  new DirectedNode("A");
       DirectedNode directedNode_b = new DirectedNode("B");
       DirectedNode directedNode_c = new DirectedNode("B");
       directedGraph.addNode(directedNode_a);
       directedGraph.addNode(directedNode_b);
       directedNode_a.addEdge(new DirectedEdge(2,directedNode_b));
       directedNode_c.addEdge(new DirectedEdge(6,directedNode_c)); 
       
       assertEquals(directedGraph.getNodeByName("A"),directedNode_a ); 
       assertEquals(directedGraph.getNodeByName("B"),directedNode_b ); 
       assertNotEquals(directedGraph.getNodeByName("C"),directedNode_c ); 

     }
    
}
