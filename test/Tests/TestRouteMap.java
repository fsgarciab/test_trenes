/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Tests;

import java.util.ArrayList;
import java.util.List;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import trainsrouteinfo.exceptions.CityDoesntExistsException;
import trainsrouteinfo.exceptions.NotAllowedWeightException;
import trainsrouteinfo.routemaps.Route;
import trainsrouteinfo.routemaps.RouteMap;

/**
 *
 * @author Excalibur
 */
public class TestRouteMap {
    
    public TestRouteMap() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    @Test(expected = CityDoesntExistsException.class)
    public void testRouteMapInputs() {
       RouteMap routes = new RouteMap();
 
       
       routes.registerCity("A");
       
       routes.registerCity("B");
       
       routes.registerDistanceBetweenCities("A","C", 5);
       
       
    }
    
    @Test
    public void testRouteMapResults() {
       RouteMap routes = new RouteMap();
 
       
       routes.registerCity("A");
       
       routes.registerCity("B");
       
       routes.registerCity("C");
       
       routes.registerCity("D");
       
       routes.registerCity("E");
       
       routes.registerDistanceBetweenCities("A","B", 5);
       routes.registerDistanceBetweenCities("B","C", 4);
       routes.registerDistanceBetweenCities("C","D", 8);
       routes.registerDistanceBetweenCities("D","C", 8);
       routes.registerDistanceBetweenCities("D","E", 6);
       routes.registerDistanceBetweenCities("A","D", 5);
       routes.registerDistanceBetweenCities("C","E", 2);
       routes.registerDistanceBetweenCities("E","B", 3);
       routes.registerDistanceBetweenCities("A","E", 7);
       
       String [] cities = new String[5];
       cities[0] = "A";
       cities[1] = "E";
       cities[2] = "B";
       cities[3] = "C";
       cities[4] = "D";
       
       
       int distance = routes.calculateDistanceInRoutes(cities);
       assertEquals(distance,22);
       
       List <ArrayList<Route>> result = routes.getPosibleRoutes("C", "C");

    }
}
