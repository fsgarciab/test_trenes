


/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Tests;

import java.util.ArrayList;
import java.util.List;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import trainsrouteinfo.exceptions.CityDoesntExistsException;
import trainsrouteinfo.exceptions.NotAllowedWeightException;
import trainsrouteinfo.routemaps.Route;
import trainsrouteinfo.routemaps.RouteMap;
import trainsrouteinfo.services.TrainRoutesService;

/**
 *
 * @author Excalibur
 */
public class TestRouteInfoService {
    TrainRoutesService trainRoutesService;
    public TestRouteInfoService() {
       
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
       trainRoutesService = new TrainRoutesService();
       
       trainRoutesService.registerCity("A");
       
       trainRoutesService.registerCity("B");
       
       trainRoutesService.registerCity("C");
       
       trainRoutesService.registerCity("D");
       
       trainRoutesService.registerCity("E");
       
       
       trainRoutesService.registerDistanceBetweenCities("A","B", 5);
       trainRoutesService.registerDistanceBetweenCities("B","C", 4);
       trainRoutesService.registerDistanceBetweenCities("C","D", 8);
       trainRoutesService.registerDistanceBetweenCities("D","C", 8);
       trainRoutesService.registerDistanceBetweenCities("D","E", 6);
       trainRoutesService.registerDistanceBetweenCities("A","D", 5);
       trainRoutesService.registerDistanceBetweenCities("C","E", 2);
       trainRoutesService.registerDistanceBetweenCities("E","B", 3);
       trainRoutesService.registerDistanceBetweenCities("A","E", 7);
    }
    
    @After
    public void tearDown() {
         trainRoutesService =  null;
    }

    @Test(expected = CityDoesntExistsException.class)
    public void testRouteMapInputs() {
       
       
       trainRoutesService.registerDistanceBetweenCities("A","Z", 5);
       
       
    }
    
    @Test
    public void testDistanceInRoutes() {
       String [] cities = new String[5];
       cities[0] = "A";
       cities[1] = "E";
       cities[2] = "B";
       cities[3] = "C";
       cities[4] = "D";

       int distance = trainRoutesService.calculateDistanceInRoutes(cities);
       assertEquals(distance,22);
    }
    
    
    @Test
    public void testNumberOfRoutes() {
        int numberofStops_bb = trainRoutesService.getNumberOfRoutesAvailable("B", "B");
        assertEquals(numberofStops_bb,2);
        
        int numberofStops_cc = trainRoutesService.getNumberOfRoutesAvailable("C", "C");
        assertEquals(numberofStops_cc,3);
    }
}
