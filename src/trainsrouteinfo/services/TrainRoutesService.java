/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package trainsrouteinfo.services;

import java.util.ArrayList;
import java.util.List;
import trainsrouteinfo.exceptions.CityDoesntExistsException;
import trainsrouteinfo.routemaps.Route;
import trainsrouteinfo.routemaps.RouteMap;

/**
 * OJO: La idea de este servicio es simular la funcionalidad de un API REST,en el
 * cual no interviene objectos como tal sino solo string para la comunicación
 * @author Excalibur
 */
public class TrainRoutesService {
    RouteMap routeMap ;

    public TrainRoutesService() {
        this.routeMap = new RouteMap();
    }
    
    public boolean registerCity(String name ){
        routeMap.registerCity(name);
        return true;
    }
    
    public boolean existsCity(String name){
        try{
        routeMap.getCity(name);
        return true;
        }catch(CityDoesntExistsException e){
            return false;
        }
    }
    
    public boolean registerDistanceBetweenCities(String originCityName, String destinyCityName, int distance ){
        routeMap.registerDistanceBetweenCities(originCityName, destinyCityName, distance);
        return true;
    }
    
    public int calculateDistanceInRoutes(String[] cities){
        return routeMap.calculateDistanceInRoutes(cities); 
    }
    
    public List <ArrayList<Route>> getPossibleRoutes(String originCityName, String destinyCityName){
        return routeMap.getPosibleRoutes(originCityName, destinyCityName);
    }
    
    public ArrayList<Route> getShortestRoute(String originCityName, String destinyCityName){
        List <ArrayList<Route>> possibleRoutes = this.getPossibleRoutes(originCityName, destinyCityName);
        
        ArrayList<Route> choosenRoute = null;
        for(ArrayList<Route> item : possibleRoutes){
          int routeLenght = this.getRouteLenght(item);
          if ( choosenRoute == null || routeLenght < this.getRouteLenght(choosenRoute)){
              choosenRoute = item;
          }
        }
        return choosenRoute;
    }
    
    private int getRouteLenght(ArrayList<Route> routes){
         int routeLenght = 0;  
         routeLenght = routes.stream().map((route) -> route.getEdgeWeight()).reduce(routeLenght, Integer::sum);
         return routeLenght;
    }
    
    public int getLenghtShortestRoute(String originCityName, String destinyCityName){
         ArrayList<Route> shortestRoute = this.getShortestRoute(originCityName,destinyCityName);
         return this.getRouteLenght(shortestRoute);
    }
    

            
   public int getNumberOfRoutesAvailable(String originCityName, String destinyCityName){
         List <ArrayList<Route>> possibleRoutes = this.getPossibleRoutes(originCityName, destinyCityName);
         return possibleRoutes.size();
    }
}
