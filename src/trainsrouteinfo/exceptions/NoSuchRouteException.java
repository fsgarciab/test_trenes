/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package trainsrouteinfo.exceptions;

/**
 *
 * @author Excalibur
 */
public class NoSuchRouteException extends RuntimeException {
    public NoSuchRouteException(){
         super("No such route" );
    }
}
