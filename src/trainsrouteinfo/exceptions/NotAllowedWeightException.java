/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package trainsrouteinfo.exceptions;


public class NotAllowedWeightException extends RuntimeException {
    public NotAllowedWeightException(String message){
         super("Not allowed number: "+ message );
    }
}