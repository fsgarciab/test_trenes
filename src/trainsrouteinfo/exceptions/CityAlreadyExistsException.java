/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package trainsrouteinfo.exceptions;

/**
 *
 * @author Excalibur
 */
public class CityAlreadyExistsException extends RuntimeException {
    public CityAlreadyExistsException(String city){
         super("This city:  \"" + city + "\" already exists" );
    }
}
