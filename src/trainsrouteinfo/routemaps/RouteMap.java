/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package trainsrouteinfo.routemaps;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import trainsrouteinfo.exceptions.CityAlreadyExistsException;
import trainsrouteinfo.exceptions.CityDoesntExistsException;
import trainsrouteinfo.exceptions.NoSuchRouteException;
import trainsrouteinfo.graphs.Edge;
import trainsrouteinfo.graphs.Node;
import trainsrouteinfo.graphs.directed.DirectedGraph;

/**
 *
 * @author Excalibur
 */
public class RouteMap  extends DirectedGraph {
    
    public boolean registerCity(String name ){
        Node existCity = this.getNodeByName(name);
        if (existCity != null){
            throw new CityAlreadyExistsException(name);
        }
        
        this.addNode(new City(name));
        return true;
    }
    
    public boolean registerDistanceBetweenCities(String originCityName, String destinyCityName, int distance ){
        Node existOriginCity = this.getCity(originCityName);
        Node existDestinyCity = this.getCity(destinyCityName);
        existOriginCity.addEdge(new Route(distance,existDestinyCity));
        return true;
    }
    
    public int calculateDistanceInRoutes(String[] cities){
        int length = cities.length;
        int distance = 0 ;
        if (length<=1){        
            return distance;  
        }
        for(int i=0; i<length; i++){
            if(i+1<length){
                 Node currentCity = this.getCity(cities[i]);
                 Map<String,Edge> routes = currentCity.getEdges();
                 Edge route = routes.get(cities[i+1]);
                 if (route == null){
                     throw new NoSuchRouteException();
                 }
                 distance += route.getEdgeWeight();
            }
        }
        return distance;
    }
    
    
    public City getCity(String name){
        City existDestinyCity = (City) this.getNodeByName(name); 
         if (existDestinyCity == null ){
            throw new CityDoesntExistsException(name);
        }  
        return existDestinyCity; 
    }
    
    public List <ArrayList<Route>> getPosibleRoutes(String originCityName, String destinyCityName){
        City existOriginCity = this.getCity(originCityName);
        City existDestinyCity = this.getCity(destinyCityName);
        City currentCity = existOriginCity;
        
        List <ArrayList<Route>> resultCitiesList;
        resultCitiesList = new ArrayList();
        
        Map<String, Route> routes = currentCity.getEdges();
        
        for (Entry<String,Route> item : routes.entrySet()){  
           ArrayList newArrayList = new ArrayList();
           resultCitiesList.add(newArrayList);
           getPosibleRoutes(existOriginCity, existDestinyCity, (Route) item.getValue(), resultCitiesList, newArrayList, 0, null  );
        }
        
        return resultCitiesList;
        
    }
    
    private void getPosibleRoutes(City originCity, City destinyCity, Route currentRoute, List <ArrayList<Route>> resulCitiesList, ArrayList<Route> currentList,  int iterationNumber, ArrayList<Route> listToRemove ){
        if(iterationNumber>100)
        {
            resulCitiesList.remove(listToRemove);
            return ;
        }
        
        currentList.add(currentRoute);
        City currentCity = (City) currentRoute.getLinkedNode();
        if (destinyCity.equals(currentCity) && listToRemove!=null){
            return ;
        }
        
        Map<String, Route> routes = currentCity.getEdges();
        for (Entry<String,Route> item : routes.entrySet()){  
           if(!this.isAlreadyVisited((City) item.getValue().getLinkedNode(), currentList)){
               ArrayList<Route> newArrayList = new ArrayList();
               currentList.stream().forEach((route) -> {
                   newArrayList.add(route);
               });
               resulCitiesList.add(newArrayList);
               getPosibleRoutes(originCity, destinyCity, (Route) item.getValue(), resulCitiesList, newArrayList, ++iterationNumber, currentList  );
           }
          
        }
        resulCitiesList.remove(currentList);
       
    }
    private boolean isAlreadyVisited(City currentCity, ArrayList<Route> currentList ){
        
         for(Route route : currentList){
            if(currentCity.equals(route.getLinkedNode())){
                return true;
            }
        }
        return false;
    }
    
}
