
package trainsrouteinfo.graphs;

import java.util.Set;

public interface Edge<N extends Node> {
    int getEdgeWeight();
    boolean setLinkedNode(N node);
    N getLinkedNode();
}
