
package trainsrouteinfo.graphs;

public interface Graph<N extends Node, E extends Edge> {
    boolean addNode(N node);
    boolean removeNode (N node);
    N getNodeByName(String name);
}
