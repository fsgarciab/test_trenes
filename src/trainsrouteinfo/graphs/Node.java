package trainsrouteinfo.graphs;
import java.util.Map;


/**
 *
 * @author Excalibur
 * @param <E>
 */
public interface Node<E> {
    Map <String,E> getEdges();
    String getName();
    boolean addEdge(E edge);
    
}
