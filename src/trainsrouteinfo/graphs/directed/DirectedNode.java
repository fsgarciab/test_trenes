/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package trainsrouteinfo.graphs.directed;

import java.util.HashMap;
import java.util.Map;
import trainsrouteinfo.graphs.Edge;
import trainsrouteinfo.graphs.Node;

/**
 *
 * @author Excalibur
 * @param <E>
 */
public  class DirectedNode<E extends Edge> implements Node<E>{
    
    final private String name ;
    final private Map <String,E> edges = new HashMap();
    
    public DirectedNode(String name){
        this.name = name;
    }
    
    @Override
    public String getName() {
        return name;
    }
    
    @Override
    public Map<String,E> getEdges() {
        return edges;
    }

    public boolean addEdge(E edge){
        edges.put(edge.getLinkedNode().getName(),edge);
        return true;
    }
    
    public boolean removeEdge(String key){
        edges.remove(key);
        return true;
    }
    
    
}
