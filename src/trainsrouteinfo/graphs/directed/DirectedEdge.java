/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package trainsrouteinfo.graphs.directed;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import trainsrouteinfo.exceptions.NotAllowedWeightException;
import trainsrouteinfo.graphs.Edge;
import trainsrouteinfo.graphs.Node;

/**
 *
 * @author Excalibur
 * @param <N>
 */
public  class DirectedEdge<N extends Node> implements Edge<N> {
    final private int edgeWeight ;
    private N linkedNode ;
    
    public DirectedEdge(int weight, N linkedNode){
        if (weight<1){
            throw new NotAllowedWeightException("The weight must be greater than 0");
        }
        this.edgeWeight = weight;
        this.linkedNode = linkedNode;
    }
 

    @Override
    public boolean setLinkedNode(N node) {
        this.linkedNode = node;
        return true;
    }

    @Override
    public int getEdgeWeight() {
        return edgeWeight;
    }

    @Override
    public N getLinkedNode() {
        return this.linkedNode;
    }


    
}
