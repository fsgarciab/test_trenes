/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package trainsrouteinfo.graphs.directed;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import trainsrouteinfo.graphs.Edge;
import trainsrouteinfo.graphs.Graph;
import trainsrouteinfo.graphs.Node;

/**
 *
 * @author Excalibur
 */
public  class DirectedGraph <N extends Node, E extends Edge> implements Graph <N,E> {
    
    Map <String, N> availableNodes = new HashMap();
    @Override
    public boolean addNode(N originNode) {
           availableNodes.put(originNode.getName(),originNode);
           return true;
    }

    @Override
    public N getNodeByName(String name) {
        return availableNodes.get(name);
    }

    @Override
    public boolean removeNode(N node) {
        availableNodes.remove(node.getName());
        return true;
    }
    
    
}
